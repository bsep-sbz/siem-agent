#!/bin/bash

# add user and group
useradd -M -p password -s /bin/bash -U siem

# login as new siem user
su siem

# create files as new siem user
touch agent.log

# set config files
cp config.example.json config.json