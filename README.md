# Siem agent application

## Installation and setup
Run `init.sh` script as a root user

```bash
$ sudo ./init.sh
```
The script will create new `siem` user, and set default config file. You can change any of the settings by editing the `config.json` file.

## Running

Run `watch.py` as new `siem` user (you will be prompted for password, it is _password_)

```bash
$ su siem
```
TODO: install dependencies, run script as siem user in background
