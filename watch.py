import os.path
import time
import copy
import requests
import json

from util import colors

def notify_api(file_name, change):
    data = {
        'file': file_name,
        'change': change
    }
    requests.post('http://localhost', data=data)


def handle_file_change(file):
    file_path = file['path']

    data = f"File {file_path} changed"
    print(colors.colors.PURPLE + colors.colors.UNDERLINE + data + colors.colors.END)
    file_changes = get_file_change_content(file)

    print(colors.colors.BLUE + 'Changes in file:' + colors.colors.END)
    print(colors.colors.GREEN + file_changes + colors.colors.END)
    print()

    set_file_line_count(file)
    # notify_api(file_path, file_changes)

def get_file_change_content(file):
    new_lines = "";
    with open(file['path']) as fp:
        for i, line in enumerate(fp):
            if i >= file['line_count']:
                new_lines += line
    return new_lines

def init_ssl(config):
    s = requests.Session()
    s.cert = (config['ssl']['cert'], config['ssl']['cert'])


def read_json_file(path):
    try:
        with open(path) as json_file:
            return json.load(json_file)
    except e:
        return {}

def set_file_line_count(file):
    file['line_count'] = get_file_line_count(file['path'])

def get_file_line_count(file_path):
    i = -1
    with open(file_path) as f:
        for i, l in enumerate(f):
          pass
        return i + 1

def set_initial_line_count(config):
    for file in config['files']:
        file['line_count'] = get_file_line_count(file['path'])

def init_conf():
    config = read_json_file('config.example.json')
    try:
        custom_config = read_json_file('config.json')
        config.update(custom_config)
    except:
        pass

    set_initial_time(config)
    set_initial_line_count(config)
    init_ssl(config)
    return config


def set_initial_time(config):
    for file in config['files']:
        try:
            file['last_modified'] = os.stat(file['path']).st_mtime
        except:
            pass


def watch_change(file):
    try:
        new_modified_at = os.stat(file['path']).st_mtime
        if new_modified_at != file['last_modified']:
            handle_file_change(file)
            file['last_modified'] = new_modified_at
    except:
        pass


def watch_all_files(conf):
    for file in conf['files']:
        watch_change(file)


def main():
    conf = init_conf()

    while True:
        time.sleep(conf['poll_interval'])
        watch_all_files(conf)


if __name__ == "__main__":
    main()
